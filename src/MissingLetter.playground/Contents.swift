import XCTest

func findMissingLetter(_ chArr: [Character]) -> Character {
    var missingChar: Character = "A"
    var previousIndex = chArr.index(chArr.startIndex, offsetBy: 0)
    for current in 1..<chArr.count {
        let currentIndex = chArr.index(chArr.startIndex, offsetBy: current)
        let previousChar = Int(String(chArr[previousIndex].asciiValue!))!
        let currentChar = Int(String((chArr[currentIndex].asciiValue!)))!
        if currentChar - previousChar != 1 {
            missingChar = Character(UnicodeScalar(currentChar - 1)!)
            break
        }
        previousIndex = currentIndex
    }
    return missingChar
}

class MissingLetter: XCTestCase {
    func testBasic() {
        XCTAssertEqual(findMissingLetter(["a","b","c","d","f"]), "e")
        XCTAssertEqual(findMissingLetter(["O","Q","R","S"]), "P")
        XCTAssertEqual(findMissingLetter(["a","b","d","f"]), "c")
        XCTAssertEqual(findMissingLetter(["X","Z"]), "Y")
        XCTAssertEqual(findMissingLetter(["a","c"]), "b")
    }
}

MissingLetter.defaultTestSuite.run()
