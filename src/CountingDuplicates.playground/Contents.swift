import XCTest

func countDuplicates(_ s:String) -> Int {
    var counts: [String : Int] = [:]
    for character in s {
        let c = String(character).lowercased()
        if counts.keys.contains(c) {
            counts[c]! += 1
        }
        else {
            counts[c] = 1
        }
    }
    var total = 0
    for (_, count) in counts {
        if count > 1 {
            total += 1
        }
    }
    return total
}

func check(_ s:String, _ c:Int) {
  XCTAssertEqual(countDuplicates(s), c)
}

class CountingDuplicates: XCTestCase {
    func testExample() {
        check("", 0)
        check("a", 0)
        check("aa", 1)
        check("aabbcde", 2)
        check("aabBcde", 2)
        check("Indivisibilities", 2)
        check("aA11", 2)
        check("ABBA", 2)
        
        check("abcde", 0)
        check("abcdea", 1)
        check("indivisibility", 1)
    }
}

CountingDuplicates.defaultTestSuite.run()
