import XCTest

func sumOfIntegersInString(_ string: String) -> Int {
    var numbers: [String] = []
    var currentChars = ""
    for char in string {
        switch char {
        case "0","1","2","3","4","5","6","7","8","9":
            currentChars += String(char)
        default:
            if currentChars.count > 0 {
                numbers.append(currentChars)
                currentChars = ""
            }
        }
    }
    if currentChars.count > 0 {
        numbers.append(currentChars)
    }
    var sum = 0
    for number in numbers {
        sum += Int(number)!
    }
    return sum
}

class SumOfIntegers: XCTestCase {
    func test() {
        XCTAssertEqual(sumOfIntegersInString(""), 0)
        XCTAssertEqual(sumOfIntegersInString("12.4"), 16)
        XCTAssertEqual(sumOfIntegersInString("h3ll0w0rld"), 3)
        XCTAssertEqual(sumOfIntegersInString("2 + 3 = "), 5)
        XCTAssertEqual(sumOfIntegersInString("Our company made approximately 1 million in gross revenue last quarter."), 1)
        XCTAssertEqual(sumOfIntegersInString("The Great Depression lasted from 1929 to 1939."), 3868)
        XCTAssertEqual(sumOfIntegersInString("Dogs are our best friends."), 0)
        XCTAssertEqual(sumOfIntegersInString("C4t5 are 4m4z1ng."), 18)
        XCTAssertEqual(sumOfIntegersInString("The30quick20brown10f0x1203jumps914ov3r1349the102l4zy dog"), 3635)
    }
}

SumOfIntegers.defaultTestSuite.run()
