import XCTest

func numericals(_ str: String) -> String {
    var counts: [Character : Int] = [:]
    var numbers = ""
    for char in str {
        if counts.keys.contains(char) {
            counts[char]! += 1
            numbers += String(counts[char]!)
        }
        else {
            counts[char] = 1
            numbers += "1"
        }
    }
    return numbers
}

class NumericalsTest: XCTestCase {
    static var allTests = [
        ("Test Basics", testBasics),
    ]


    func testBasics() {
        XCTAssertEqual(numericals("Hello, World!"), "1112111121311")
        XCTAssertEqual(numericals("Hello, World! It's me, JomoPipi!"), "11121111213112111131224132411122")
        XCTAssertEqual(numericals("hello hello"), "11121122342")
        XCTAssertEqual(numericals("Hello"), "11121")
        XCTAssertEqual(numericals("11111"), "12345")
        XCTAssertEqual(numericals("hope you 123456789 expected numbers in the string"), "1111112121111111113212311414121151151262267232231")
        XCTAssertEqual(numericals("In this string, I'll make sure the amounts of a character go over 9"), "11111112221221132112411115312263237221234482193101343525441123124155131")
    }

}

NumericalsTest.defaultTestSuite.run()
