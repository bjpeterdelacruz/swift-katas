import XCTest

func findDigit(_ num:Int, _ nth: Int) -> Int {
    if nth < 1 {
        return -1
    }
    var str = String(num)
    if str.contains("-") {
        str = str.replacingOccurrences(of: "-", with: "")
    }
    if str.count < nth {
        return 0
    }
    str = String(str.reversed())
    let idx = str.index(str.startIndex, offsetBy: nth - 1)
    return Int(String(str[idx]))!
}

class FindNthDigitTest: XCTestCase {
    static var allTests = [
        ("testNormalValues", testNormalValues),
        ("testNumIsNegative", testNumIsNegative),
        ("testNthNotPositive", testNthNotPositive),
    ]

    func testNormalValues() {
        XCTAssertEqual(findDigit(6429, 3), 4);
    }
    
    func testNumIsNegative() {
        XCTAssertEqual(findDigit(-1234, 2), 3);
    }
    
    func testNthNotPositive() {
        XCTAssertEqual(findDigit(678998, 0), -1);
    }
    
    func testExamples() {
        XCTAssertEqual(findDigit(5673, 1), 3);
        XCTAssertEqual(findDigit(5673, 2), 7);
        XCTAssertEqual(findDigit(5673, 3), 6);
        XCTAssertEqual(findDigit(5673, 4), 5);
        XCTAssertEqual(findDigit(5673, 5), 0);
        XCTAssertEqual(findDigit(129, 2), 2);
        XCTAssertEqual(findDigit(-2825, 3), 8);
        XCTAssertEqual(findDigit(-456, 3), 4);
        XCTAssertEqual(findDigit(-456, 4), 0);
        XCTAssertEqual(findDigit(0, 20), 0);
        XCTAssertEqual(findDigit(65, 0), -1);
        XCTAssertEqual(findDigit(24, -8), -1);
    }
}

FindNthDigitTest.defaultTestSuite.run()
