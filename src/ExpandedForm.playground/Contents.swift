import XCTest

func expandedForm(_ num: Int) -> String {
    let s = String(num)
    if s.count == 1 {
        return s
    }
    var arr: [String] = []
    for offset in 0..<s.count {
        let head = String(s[s.index(s.startIndex, offsetBy: offset)])
        if head == "0" {
            continue
        }
        let tail = s[s.index(s.startIndex, offsetBy: offset + 1)...]
        arr.append(head + String(repeating: "0", count: tail.count))
    }
    return arr.joined(separator: " + ")
}

class ExpandedForm: XCTestCase {
    static let testValues = [
        (0, "0"),
        (1, "1"),
        (10, "10"),
        (12, "10 + 2"),
        (42, "40 + 2"),
        (100, "100"),
        (101, "100 + 1"),
        (110, "100 + 10"),
        (111, "100 + 10 + 1"),
        (70304, "70000 + 300 + 4")
    ]
    
    func testExpandedForm() {
        for test in ExpandedForm.testValues {
            XCTAssertEqual(expandedForm(test.0), test.1)
        }
    }
}

ExpandedForm.defaultTestSuite.run()
