import XCTest

func whatCentury(_ year: String) -> String {
    if year.count < 3 {
        return "1st"
    }
    let ending = Int(year)! % 100 == 0 ? 0 : 1
    let index = year.index(year.startIndex, offsetBy: year.count == 3 ? 0 : 1)
    let prefix = String(Int(year[...index])! + ending)
    switch prefix {
    case "11":
        return "11th"
    case "12":
        return "12th"
    case "13":
        return "13th"
    default:
        break
    }
    switch prefix.last! {
    case "1":
        return "\(prefix)st"
    case "2":
        return "\(prefix)nd"
    case "3":
        return "\(prefix)rd"
    default:
        return "\(prefix)th"
    }
}

class WhatCentury: XCTestCase {
    func testExample() {
        XCTAssertEqual("9th", whatCentury("850"), "With input '850' solution produced wrong output.")
        
        XCTAssertEqual("1st", whatCentury("14"), "With input '0014' solution produced wrong output.")
        XCTAssertEqual("1st", whatCentury("100"), "With input '0100' solution produced wrong output.")
        XCTAssertEqual("2nd", whatCentury("101"), "With input '0101' solution produced wrong output.")
        XCTAssertEqual("2nd", whatCentury("200"), "With input '0201' solution produced wrong output.")
        XCTAssertEqual("3rd", whatCentury("201"), "With input '0201' solution produced wrong output.")
        XCTAssertEqual("20th", whatCentury("2000"), "With input '2000' solution produced wrong output.")
        
        XCTAssertEqual("20th", whatCentury("1999"), "With input '1999' solution produced wrong output.")
        XCTAssertEqual("21st", whatCentury("2011"), "With input '2011' solution produced wrong output.")
        XCTAssertEqual("22nd", whatCentury("2154"), "With input '2154' solution produced wrong output.")
        XCTAssertEqual("23rd", whatCentury("2259"), "With input '2259' solution produced wrong output.")
    }
}

WhatCentury.defaultTestSuite.run()
