import XCTest

func digPow(for number: Int, using power: Int) -> Int {
    var sum = 0
    var pow = power
    for char in String(number) {
        var result = 1
        var exp = 1
        while exp <= pow {
            result *= Int(String(char))!
            exp += 1
        }
        sum += result
        pow += 1
    }
    return sum % number == 0 ? sum / number : -1
}

class PlayingWithDigits: XCTestCase {
    func testDigPow() {
        XCTAssertEqual(digPow(for: 89, using: 1), 1)
        XCTAssertEqual(digPow(for: 89, using: 2), -1)
        XCTAssertEqual(digPow(for: 92, using: 1), -1)
        XCTAssertEqual(digPow(for: 695, using: 2), 2)
        XCTAssertEqual(digPow(for: 46288, using: 3), 51)
        XCTAssertEqual(digPow(for: 114, using: 3), 9)
    }
}

PlayingWithDigits.defaultTestSuite.run()
