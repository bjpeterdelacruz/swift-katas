import XCTest

func toNato(_ words: String) -> String {
    var nato: [String: String] = [:]
    nato = ["A": "Alfa", "B": "Bravo", "C": "Charlie", "D": "Delta", "E": "Echo",
            "F": "Foxtrot", "G": "Golf", "H": "Hotel", "I": "India", "J": "Juliett",
            "K": "Kilo", "L": "Lima", "M": "Mike", "N": "November", "O": "Oscar",
            "P": "Papa", "Q": "Quebec", "R": "Romeo", "S": "Sierra", "T": "Tango",
            "U": "Uniform", "V": "Victor", "W": "Whiskey", "X": "Xray", "Y": "Yankee",
            "Z": "Zulu"]
    var characters: [String] = []
    for char in words {
        if nato.keys.contains(char.uppercased()) {
            characters.append(nato[char.uppercased()]!)
        }
        else if char.isPunctuation {
            characters.append(String(char))
        }
    }
    return characters.joined(separator: " ")
}

class IfYouCanReadThisTest: XCTestCase {
    static var allTests = [
        ("Test Example", testToNato),
    ]

    func testToNato() {
        XCTAssertEqual(toNato("If you can read?"), "India Foxtrot Yankee Oscar Uniform Charlie Alfa November Romeo Echo Alfa Delta ?")
        XCTAssertEqual(toNato("Did not see that coming."), "Delta India Delta November Oscar Tango Sierra Echo Echo Tango Hotel Alfa Tango Charlie Oscar Mike India November Golf .")
        XCTAssertEqual(toNato("go for it!"), "Golf Oscar Foxtrot Oscar Romeo India Tango !")
        
        XCTAssertEqual(toNato("  abc  def  ghi  jkl  mno  pqr  stu  vwx  yz  "),
                       "Alfa Bravo Charlie Delta Echo Foxtrot Golf Hotel India Juliett Kilo Lima Mike November Oscar Papa Quebec Romeo Sierra Tango Uniform Victor Whiskey Xray Yankee Zulu")
    }
}

IfYouCanReadThisTest.defaultTestSuite.run()
