import XCTest

func disariumNumber(_ number: Int) -> String {
    var sum = 0
    var pow = 1
    for char in String(number) {
        var result = 1
        var exp = 1
        while exp <= pow {
            result *= Int(String(char))!
            exp += 1
        }
        sum += result
        pow += 1
    }
    return sum == number ? "Disarium !!" : "Not !!"
}

class DisariumNumber: XCTestCase {
    func testDisariumNumber() {
      XCTAssertEqual(disariumNumber(89), "Disarium !!")
      XCTAssertEqual(disariumNumber(564), "Not !!")
      XCTAssertEqual(disariumNumber(1024), "Not !!")
      XCTAssertEqual(disariumNumber(135), "Disarium !!")
      XCTAssertEqual(disariumNumber(136586), "Not !!")
    }
}

DisariumNumber.defaultTestSuite.run()
