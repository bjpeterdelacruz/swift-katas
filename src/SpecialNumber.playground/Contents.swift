import XCTest

func specialNumber(_ number: Int) -> String {
    for char in String(number) {
        switch Int(String(char))! {
        case 0...5:
            break
        default:
            return "NOT!!"
        }
    }
    return "Special!!"
}

class SpecialNumber: XCTestCase {
    func testSpecialNumber() {
      for number in [2, 3, 11, 55, 25432] {
        XCTAssertEqual(specialNumber(number), "Special!!")
      }
      for number in [2789, 6, 9, 26, 92] {
        XCTAssertEqual(specialNumber(number), "NOT!!")
      }
    }
}

SpecialNumber.defaultTestSuite.run()
