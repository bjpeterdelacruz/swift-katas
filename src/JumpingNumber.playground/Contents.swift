import XCTest

func jumpingNumber(_ number: Int) -> String {
    var arr: [Int] = []
    for current in String(number) {
        arr.append(Int(String(current))!)
    }
    var previous = 0
    for current in 1..<arr.endIndex {
        if abs(arr[current] - arr[previous]) != 1 {
            return "Not!!"
        }
        previous = current
    }
    return "Jumping!!"
}

class JumpingNumber: XCTestCase {
    func testJumpingNumber() {
        for number in [1, 7, 9, 23, 32, 98, 8987, 4343456, 98789876] {
          XCTAssertEqual(jumpingNumber(number), "Jumping!!")
        }
        for number in [79, 100, 33] {
            XCTAssertEqual(jumpingNumber(number), "Not!!")
        }
    }
}

JumpingNumber.defaultTestSuite.run()
