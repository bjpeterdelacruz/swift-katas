import XCTest

func merge<Key, Value>(_ dicts: [[Key: Value]]) -> [Key: [Value]] {
    var combinedDict: [Key: [Value]] = [:]
    for dict in dicts {
        for (key, value) in dict {
            if combinedDict.keys.contains(key) {
                combinedDict[key]!.append(value)
            }
            else {
                combinedDict[key] = [value]
            }
        }
    }
    return combinedDict
}

func AssertDictionariesEqual<Key, Value: Comparable>(_ lhs: [Key: [Value]], _ rhs: [Key: [Value]], _ message: String) {
    XCTAssertEqual(lhs.count, rhs.count, "Dictionaries weren't the same length in test \(message)")
    for (key, value) in rhs {
        XCTAssertTrue(
        (lhs[key] ?? []).sorted() == value.sorted(),
        "Values at \(key) weren't equal in test, \(lhs[key].map(String.init) ?? "nil") != \(value), \(message)"
        )
    }
}

class DictionaryMerge: XCTestCase {
    func testSimpleTests() {
        AssertDictionariesEqual(merge([[:], [:], [:]] as [[AnyHashable: Int]]), [:], "Merging empty dictionaries should return an empty dictionary")
        AssertDictionariesEqual(merge([["A": 1, "B": 2, "C": 3]]), ["A": [1], "B": [2], "C": [3]], "Merging a single dictionary should return a dictionary with the same content")
        AssertDictionariesEqual(merge([["A": 1], ["B": 2]]), ["A": [1], "B": [2]], "Merging two simple dictionaries combines them")
        AssertDictionariesEqual(
            merge([["A": 1, "B": 2, "C": 3], ["A": 4, "D": 5]]),
            ["A": [1, 4], "B": [2], "C": [3], "D": [5]],
            "Merging two dictionaries with multiple values returns a combined dictionary"
        )
        AssertDictionariesEqual(
            merge([["A": 1, "B": 2, "C": 3], ["A": 5, "D": 6], ["E": 7, "F": 8], ["E": 9, "A": 1]]),
            ["A": [1, 5, 1], "B": [2], "C": [3], "D": [6], "E": [7, 9], "F": [8]],
            "Merging multiple dictionaries returns a combined dictionary"
        )
        AssertDictionariesEqual(
            merge([["A": 1, "B": 2, "C": 3], ["A": 4, "D": 5], [:], ["E": 6, "D": 7]]),
            ["A": [1, 4], "B": [2], "C": [3], "D": [5, 7], "E": [6]],
            "Merging multiple dictionaries with some empty returns a combined dictionary"
        )
    }
}

DictionaryMerge.defaultTestSuite.run()
