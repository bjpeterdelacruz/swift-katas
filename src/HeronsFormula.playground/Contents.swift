import XCTest

func heron(_ a: Double, _ b: Double, _ c: Double) -> Double {
    let s = (a + b + c) / 2
    let product = s * (s - a) * (s - b) * (s - c)
    return round(product.squareRoot() * 100) / 100
}

class HeronsFormula: XCTestCase {
    func testHeron() {
        XCTAssertEqual(heron(3, 4, 5), 6)
        XCTAssertEqual(heron(6, 8, 10), 24)
        XCTAssertEqual(heron(8, 9, 14), 33.67)
    }
}

HeronsFormula.defaultTestSuite.run()
