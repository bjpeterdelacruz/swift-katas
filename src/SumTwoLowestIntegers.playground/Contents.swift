import XCTest

func sumOfTwoSmallestIntegersIn(_ array: [Int]) -> Int {
    var lowest: Int, secondLowest: Int
    if array[0] < array[1] {
        lowest = array[0]
        secondLowest = array[1]
    }
    else {
        lowest = array[1]
        secondLowest = array[0]
    }
    for idx in 2..<array.count {
        if array[idx] < lowest {
            secondLowest = lowest
            lowest = array[idx]
        }
        else if array[idx] < secondLowest {
            secondLowest = array[idx]
        }
    }
    return lowest + secondLowest
}

class SumTwoLowestIntegersTest: XCTestCase {
    static var allTests = [
        ("Basic tests", testSum),
    ]

    func testSum() {
        XCTAssertEqual(sumOfTwoSmallestIntegersIn([5, 8, 12, 18, 22]), 13)
        XCTAssertEqual(sumOfTwoSmallestIntegersIn([7, 15, 12, 18, 22]), 19)
        XCTAssertEqual(sumOfTwoSmallestIntegersIn([25, 42, 12, 18, 22]), 30)
        XCTAssertEqual(sumOfTwoSmallestIntegersIn([1, 8, 12, 18, 5]), 6)
        XCTAssertEqual(sumOfTwoSmallestIntegersIn([13, 12, 5, 61, 22]), 17)
    }
}

SumTwoLowestIntegersTest.defaultTestSuite.run()
