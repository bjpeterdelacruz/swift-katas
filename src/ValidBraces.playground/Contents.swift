import XCTest

func validBraces(_ string:String) -> Bool {
    var lastSeenCharacters: [String] = []
    for char in string {
        switch char {
        case "[":
            lastSeenCharacters.append("[")
        case "]":
            if lastSeenCharacters.last != "[" {
                return false
            }
            lastSeenCharacters.popLast()
        case "{":
            lastSeenCharacters.append("{")
        case "}":
            if lastSeenCharacters.last != "{" {
                return false
            }
            lastSeenCharacters.popLast()
        case "(":
            lastSeenCharacters.append("(")
        case ")":
            if lastSeenCharacters.last != "(" {
                return false
            }
            lastSeenCharacters.popLast()
        default:
            break
        }
    }
    return lastSeenCharacters.isEmpty
}

class ValidBraces: XCTestCase {
    func testNonNested() {
        XCTAssertTrue(validBraces("()"))
        XCTAssertTrue(validBraces("[]"))
        XCTAssertTrue(validBraces("{}"))
        XCTAssertTrue(validBraces("(){}[]"))
    }
    
    func testNested() {
        XCTAssertTrue(validBraces("([{}])"))
        XCTAssertFalse(validBraces("(}"))
        XCTAssertFalse(validBraces("[(])"))
        XCTAssertTrue(validBraces("({})[({})]"))
        XCTAssertFalse(validBraces("(})"))
        XCTAssertTrue(validBraces("(({{[[]]}}))"))
        XCTAssertTrue(validBraces("{}({})[]"))
        XCTAssertFalse(validBraces(")(}{]["))
        XCTAssertFalse(validBraces("())({}}{()][]["))
        XCTAssertFalse(validBraces("(((({{"))
        XCTAssertFalse(validBraces("}}]]))}])"))
    }
}

ValidBraces.defaultTestSuite.run()
