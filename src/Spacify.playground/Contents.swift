import XCTest

func spacify(_ str: String) -> String {
    return str.map { String($0) }.joined(separator: " ")
}

class SpacifyTest: XCTestCase {
    static var allTests = [
        ("Basic Tests", testBasics),
    ]

    func testBasics() {
        XCTAssertEqual(spacify("hello world"), "h e l l o   w o r l d")
        XCTAssertEqual(spacify("12345"),"1 2 3 4 5")
        XCTAssertEqual(spacify(""), "")
        XCTAssertEqual(spacify("a"),"a")
        XCTAssertEqual(spacify("Pippi"),"P i p p i")
    }

}

SpacifyTest.defaultTestSuite.run()
