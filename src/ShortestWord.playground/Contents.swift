import XCTest

func find_short(_ str: String) -> Int {
    var length = Int.max
    for word in str.split(whereSeparator: { $0 == " " || $0 == "\t" || $0 == "\n" }) {
        if word.count < length {
            length = word.count
        }
    }
    return length
}

class ShortestWord: XCTestCase {
    static var allTests = [
        ("Test Example", testExample),
    ]

    func testExample() {
        XCTAssertEqual(3, find_short("bitcoin take over the world maybe who knows perhaps"))
        XCTAssertEqual(3, find_short("bitcoin \t take over\nthe world maybe who knows perhaps"))
        XCTAssertEqual(3, find_short("lets talk about javascript the best language"))
        XCTAssertEqual(1, find_short("i want to travel the world writing code one day"))
        XCTAssertEqual(2, find_short("Lets all go on holiday somewhere very cold"))
    }
}

ShortestWord.defaultTestSuite.run()
