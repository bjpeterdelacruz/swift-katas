import XCTest

func replaceAll<T: Equatable>(array: [T], old: T, new: T) -> [T] {
    return array.map { $0 == old ? new : $0 }
}

class ReplaceAllItems: XCTestCase {
    func testReplaceAll() {
        XCTAssertEqual(replaceAll(array: [1,2,2], old: 1, new: 2), [2,2,2])
        XCTAssertEqual(replaceAll(array: ["ooh", "la", "la"], old: "la", new: "baby"), ["ooh", "baby", "baby"])
    }
}

ReplaceAllItems.defaultTestSuite.run()
